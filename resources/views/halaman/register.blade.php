<!DOCTYPE html>
<html>
<head>
<title>Belajar HTML</title>
</head>
<body>

<h1>Buat Account Baru !</h1>
<h3>Sign Up Form</h3>

 <form action="/welcome" method="post">
  @csrf
  <label>First name:</label><br><br>
  <input type="text" name="firstname"><br><br>
  <label>Last name:</label><br>
  <input type="text" name="lastname"><br><br>
  <label>Gender:</label><br><br>
  <input type="radio" name="gender" value="Male">
  <label>Male</label><br><br>
  <input type="radio" name="gender" value="Female">
  <label>Female</label><br><br>
  <input type="radio" id="Others" name="gender" value="Others">
  <label>Others</label><br><br>

  <label>Nationality</label><br><br>

  <select name="nationality" id="nationality">
  <option value="Indonesian">Indonesian</option>
</select>
<br><br>
  <label>Language Spoken:</label><br><br>
  <input type="checkbox" id="bahasa" name="language" value="Bahasa Indonesia">
  <label>Bahasa Indonesia</label><br>
  <input type="checkbox" id="english" name="language" value="English">
  <label>English</label><br>
  <input type="checkbox" id="other" name="Others" value="Others">
  <label>Others</label><br><br>
  <label>Bio :</label><br><br>
  <textarea id="bio" name="bio" rows="5" cols="30"></textarea><br><br>
  <input type="submit" value="Sign Up">
</form> 

</body>
</html>